#!/usr/bin/env bash

. dotfiles.sh

shopt -s extglob

[ -z "$HOSTNAME" ] && HOSTNAME="$(cat /etc/hostname)"

mkdir -p "$HOME/.config/sway/$HOSTNAME" "$HOME/.config/sway/conf.d" \
    "$HOME/.config/i3/$HOSTNAME" "$HOME/.config/i3/conf.d" \
    "$HOME/.config/nvim/autoload"
touch "$HOME/.profile-$HOSTNAME"

deployfiles "$HOME" ".bashrc" ".inputrc" ".profile" ".config/sway/!($HOSTNAME)" \
    ".vimperatorrc" ".hedrc" ".radare2rc" ".Xresources" "powersave" \
    "shred.sh" "*.conf" ".bash_logout" ".config/i3status" \
    ".weechat/perl/*.pl" ".weechat/python/*.py" ".weechat/lua/*.lua" \
    ".weechat/!(irc).conf" ".weechat/perl/autoload" ".config/i3/!($HOSTNAME)" \
    ".weechat/python/autoload" ".weechat/lua/autoload" ".config/ranger" \
    ".config/nvim/!(*plugged*)/*" ".config/nvim/!(plug).vim"

! [ -f "$HOME/.config/nvim/autoload/plug.vim" ] && \
    wget -O "$HOME/.config/nvim/autoload/plug.vim" \
        "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"

