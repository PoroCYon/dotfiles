
copy1() {
    local SRC="$1"
    local DST="$2"

    [[ "$SRC" =~ ".*\/" ]] && SRC="$(basename "$SRC")"
    [[ "$DST" =~ ".*\/" ]] && DST="$(basename "$DST")"

    #echo "$SRC -> $DST"

    if [ -d "$SRC" ]; then
        mkdir -pv "$DST"
        find "$SRC" -mindepth 1 -maxdepth 1 | while read -r f; do
            copy1 "$f" "$DST/$(basename "$f")"
        done
    else
        local DIR="$(dirname "$DST")"
        ! [ -d "$DIR" ] && mkdir -pv "$DIR"

        if [ -L "$SRC" ]; then
            ln -vfsT "$(readlink "$SRC")" "$DST"
        elif [ -f "$SRC" ]; then
            cp -vf "$SRC" "$DST"
        fi
    fi
}

copyglob() {
    local SRC="$1"
    local DST="$2"
    shopt -s extglob
    local REL=($SRC/$3)

    #echo "| $SRC///$3 -> $DST///$3"

    for fil in "${REL[@]}"; do
        #echo "< $fil"
        ! [ -e "$fil" ] && continue
        [ -z "$fil" ] && continue
        #echo "> $fil"

        local DSTF="$(expr substr "$fil" "$(expr "${#SRC}" + 2)" 99999)"
        copy1 "$fil" "$DST/$DSTF"
    done
}

mvfiles() {
    local SRC="$1"
    shift
    local DST="$1"
    shift
    #echo "S: $SRC ; D: $DST"

    for fil in "$@"; do
        copyglob "$SRC" "$DST" "$fil"
    done
}
export -f mvfiles

deployfiles() {
    local DSTDIR="$1"
    local SRCDIR="$(realpath "$(dirname "$0")")"

    shift

    mvfiles "$SRCDIR" "$DSTDIR" "$@"
}
export -f deployfiles
getfiles() {
    local SRCDIR="$1"
    local DSTDIR="$(realpath "$(dirname "$0")")"

    shift

    mvfiles "$SRCDIR" "$DSTDIR" "$@"
}
export -f getfiles

