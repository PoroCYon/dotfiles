
[ -f /etc/skel/.bashrc ] && . /etc/skel/.bashrc
[ -f /etc/bash/bashrc ] && . /etc/bash/bashrc

[ -f ~/.profile ] && . ~/.profile
[ -f /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

