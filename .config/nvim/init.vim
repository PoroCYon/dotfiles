set nocompatible

filetype off
filetype plugin indent on
syntax enable

call plug#begin('~/.config/nvim/plugged')

Plug 'altercation/vim-colors-solarized'
Plug 'kongo2002/fsharp-vim', { 'for': 'fsharp' }
"Plug 'idris-hackers/idris-vim', { 'for': 'idris,idr,lidr' }
Plug 'airblade/vim-gitgutter'
"Plug 'jamessan/vim-gnupg'
Plug 'sheerun/vim-polyglot'
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'itchyny/lightline.vim'
"Plug 'lervag/vimtex', { 'for': 'tex,plaintex' }
Plug 'jiangmiao/auto-pairs'
Plug 'xtal8/traces.vim'
Plug 'sirtaj/vim-openscad', { 'for': 'openscad' }
Plug 'ARM9/arm-syntax-vim', { 'for': 'asm' }
Plug 'gryf/kickass-syntax-vim', { 'for': 'asm' }
Plug 'Shirk/vim-gas', { 'for': 'asm' }
"Plug 'emdeeeks/vim-titansi'

call plug#end()

set tabstop=4
"set softtabstop=4
set noexpandtab
set shiftwidth=4
set autoindent
set smartindent
set cindent
set smarttab
set scrolloff=8
set modeline


set ruler
set number
"set lazyredraw
set showmatch
set nowrap
set cursorline
set splitbelow
set showcmd


if $TERM_PROGRAM =~ "rxvt-256color" || $TERM_PROGRAM =~ "screen-256color" || $TERM_PROGRAM =~ "xterm-termite" || $TERM_PROGRAM =~ "xterm-kitty"
    "let g:solarized_termcolors=256
    let g:solarized_visibility="normal"
    let g:solarized_bold=1
    let g:solarized_italic=1
    let g:solarized_underline=1
    let g:solarized_hitrail=1
    let g:solarized_termtrans=1
    colorscheme solarized
    set background=dark

    let g:lightline = {
      \ 'colorscheme': 'solarized',
      \ 'component': {
      \   'readonly': '%{&readonly?"":""}',
      \ },
      \ 'separator': { 'left': '', 'right': '' },
      \ 'subseparator': { 'left': '', 'right': '' }
      \ }
else
   let g:lightline = {
      \ 'component': {
      \   'readonly': '%{&readonly?"":""}',
      \ },
      \ 'separator': { 'left': '', 'right': '' },
      \ 'subseparator': { 'left': '', 'right': '' }
      \ }

endif

set cursorline


set ignorecase
set smartcase
set hlsearch
set incsearch
set magic
set mat=2
set colorcolumn=80


set history=0
set undolevels=1000
set visualbell
set noerrorbells

set pastetoggle=<F2>

let mapleader=" "
let g:mapleader=" "
let maplocalleader=","
let g:maplocalleader=","
let g:airline_powerline_fonts=1

set foldmethod=indent
set foldnestmax=10
set foldlevelstart=99
set foldcolumn=0

set autoread

set formatprg="PARINIT='rTbgqR B=.,?_A_a Q=_s>|' par\ -w72"

set list
if &listchars ==# 'eol:$'
  set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
endif

set cmdheight=1
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

set mouse=a

set nobackup
set wb
set noswapfile
"set expandtab
set laststatus=2
set hidden

set ffs=unix,dos,mac
set hls!

noremap ,, ,
" ';' is replaced with 'm', 'ù' is right next to 'm' (under '[')
nnoremap ù :

nmap <leader>nt :NERDTreeToggle<cr>

" AZERTY
" hjlk -> jklm ('m' is right next to 'l' and under 'p')
nnoremap m l
nnoremap l k
nnoremap k j
nmap     j h
vnoremap m l
vnoremap l k
vnoremap k j
vmap     j h

nnoremap M <S-Right>
nnoremap L <S-Up>
nnoremap K <S-Down>
nmap     J <S-Left>

nmap e <End>
nmap c <Home>
vmap e <End>
vmap c <Home>

nnoremap Q :qa!<cr>

inoremap df <Esc>
nnoremap ² <Esc>
nnoremap <silent> df <Ins>

"nnoremap rt xp

nnoremap <leader>v V`]
nnoremap <leader>ev :vsplit $MYVIMRC<cr>

au FocusLost * :wa

map <C-j> <C-W>h
map <C-k> <C-W>j
map <C-l> <C-W>k
map <C-m> <C-W>l

tnoremap <S-j> <C-\><C-n><C-w>h
tnoremap <S-k> <C-\><C-n><C-w>j
tnoremap <S-l> <C-\><C-n><C-w>k
tnoremap <S-m> <C-\><C-n><C-w>l

nmap <leader>w :w!<cr>
"nmap <leader>nh :noh<cr>

nnoremap <leader>ma :set mouse=a<cr>
nnoremap <leader>mo :set mause=<cr>

nmap <leader>sj :leftabove  vnew<cr>
nmap <leader>sm :rightbelow vnew<cr>
nmap <leader>sk :leftabove   new<cr>
map  <leader>sl :rightbelow  new<cr>

nnoremap <leader>bp :bp<cr>
nnoremap <leader>bn :bn<cr>
noremap <leader>bd :Bd<cr>

"setglobal fileencoding=utf-8

syn keyword globalTodo TODO FIXME XXX HACK NOTE contained
highlight def link globalTodo Todo

au BufNewFile,Bufread *.s,*.S set ft=asm
au BufNewFile,BufRead *.asm set ft=nasm

au BufNewFile,BufRead *.fs,*.fsx,*.fsi  set filetype=fsharp
au BufNewFile,BufRead *.ats,*.dats,*.cats,*.hats    set filetype=ats
au BufNewFile,BufRead *.il set filetype=msil
" good enough
au BufNewFile,BufRead *.xsh set filetype=python
au BufNewFile,BufRead *.glsl set filetype=glsl

au BufNewFile,BufRead *.sspt set filetype=sh

" xbps-src packages
au BufNewFile,BufRead template
            \ setlocal noet |
            \ setlocal ts=4 |
            \ setlocal ft=sh

" Indent .myr and .go files with tabs
autocmd FileType myr
            \ setlocal noet |
            \ setlocal ts=4
autocmd FileType go
            \ setlocal noet |
            \ setlocal ts=4
autocmd FileType html
            \ setlocal noet |
            \ setlocal ts=2 |
            \ setlocal sw=2
autocmd FileType xml
            \ setlocal noet |
            \ setlocal ts=2 |
            \ setlocal sw=2
autocmd FileType arduino
            \ setlocal ts=2 |
            \ setlocal sw=2

"set shortmess+=c

