
" MSIL syntax highlighting written by someone on the Vanilla Internetworks(R)
" NOTE: I (PoroCYon) have modified it quite a bit so it highlights all MSIL
" keywords etc.

if version < 600
    syntax clear
elseif exists("b:current_syntax")
    finish
endif

" *NOT* Case insensitive
"syntax case ignore

" The directives

syntax match ilasmDirective "\.override"
syntax match ilasmDirective "\.namespace"
syntax match ilasmDirective "\.method"
syntax match ilasmDirective "\.class"
syntax match ilasmDirective "\.field"
syntax match ilasmDirective "\.locals"
syntax match ilasmDirective "\.entrypoint"
syntax match ilasmDirective "\.module"
syntax match ilasmDirective "\.data"
syntax match ilasmDirective "\.size"
syntax match ilasmDirective "\.property"
syntax match ilasmDirective "\.get"
syntax match ilasmDirective "\.set"
syntax match ilasmDirective "\.other"
syntax match ilasmDirective "\.permission"
syntax match ilasmDirective "\.permissionset"
syntax match ilasmDirective "\.custom"
syntax match ilasmDirective "\.subsystem"
syntax match ilasmDirective "\.corflags"
syntax match ilasmDirective "\.file\salignement"
syntax match ilasmDirective "\.imagebase"
syntax match ilasmDirective "\.line"
syntax match ilasmDirective "\.language"
syntax match ilasmDirective "\.hash\salgorithm"
syntax match ilasmDirective "\.ver"
syntax match ilasmDirective "\.maxstack"
syntax match ilasmDirective "\.file\salignment"
syntax match ilasmDirective "\.stackreserve"
syntax match ilasmDirective "\.publickeytoken"
syntax match ilasmDirective "pinned"
syntax match ilasmDirective "\.ctor"
syntax match ilasmDirective "\.cctor"
syntax match ilasmDirective "\.assembly"

" The types

syntax keyword ilasmType int8 uint8 int32 uint32 int64 float32 float64 string void int16 char bool bytearray nullref unsigned object typedref native int uint


" The keywords
"syntax match ilasmKeyword "\.ctor"
"syntax match ilasmKeyword "\.cctor"
"syntax match ilasmKeyword "\.pinned"
syntax match ilasmKeyword "\s*refanytype\s*"
syntax match ilasmKeyword "\s*sizeof\s*"
syntax match ilasmKeyword "\s*rethrow\s*"
syntax match ilasmKeyword "\s*initblk\s*"
syntax match ilasmKeyword "\s*cpblk\s*"
syntax match ilasmKeyword "\s*initobj\s*"
syntax match ilasmKeyword "\s*tail\.\s*"
syntax match ilasmKeyword "\s*volatile\.\s*"
syntax match ilasmKeyword "\s*unaligned\.\s*"
syntax match ilasmKeyword "\s*endfilter\s*"
syntax match ilasmKeyword "\s*localloc\s*"
syntax match ilasmKeyword "\s*stloc\s*"
syntax match ilasmKeyword "\s*ldloca\s*"
syntax match ilasmKeyword "\s*ldloc\s*"
syntax match ilasmKeyword "\s*starg\s*"
syntax match ilasmKeyword "\s*ldarga\s*"
syntax match ilasmKeyword "\s*ldarg\s*"
syntax match ilasmKeyword "\s*ldvirtftn\s*"
syntax match ilasmKeyword "\s*ldftn\s*"
syntax match ilasmKeyword "\s*clt\.un\s*"
syntax match ilasmKeyword "\s*clt\s*"
syntax match ilasmKeyword "\s*cgt\.un\s*"
syntax match ilasmKeyword "\s*cgt\s*"
syntax match ilasmKeyword "\s*ceq\s*"
syntax match ilasmKeyword "\s*arglist\s*"
syntax match ilasmKeyword "\s*conv\.u\s*"
syntax match ilasmKeyword "\s*stind\.i\s*"
syntax match ilasmKeyword "\s*leave\.s\s*"
syntax match ilasmKeyword "\s*leave\s*"
syntax match ilasmKeyword "\s*endfault\s*"
syntax match ilasmKeyword "\s*endfinally\s*"
syntax match ilasmKeyword "\s*sub\.ovf\.un\s*"
syntax match ilasmKeyword "\s*sub\.ovf\s*"
syntax match ilasmKeyword "\s*mul\.ovf\.un\s*"
syntax match ilasmKeyword "\s*mul\.ovf\s*"
syntax match ilasmKeyword "\s*add\.ovf\.un\s*"
syntax match ilasmKeyword "\s*add\.ovf\s*"
syntax match ilasmKeyword "\s*conv\.ovf\.u\s*"
syntax match ilasmKeyword "\s*conv\.ovf\.i\s*"
syntax match ilasmKeyword "\s*conv\.i\s*"
syntax match ilasmKeyword "\s*conv\.u1\s*"
syntax match ilasmKeyword "\s*conv\.u2\s*"
syntax match ilasmKeyword "\s*ldtoken\s*"
syntax match ilasmKeyword "\s*mkrefany\s*"
syntax match ilasmKeyword "\s*ckfinite\s*"
syntax match ilasmKeyword "\s*refanyval\s*"
syntax match ilasmKeyword "\s*conv\.ovf\.[ui][1248]\s*"
syntax match ilasmKeyword "\s*stelem\.ref\s*"
syntax match ilasmKeyword "\s*stelem\.r[48]\s*"
syntax match ilasmKeyword "\s*stelem\.i[1248]\?\s*"
syntax match ilasmKeyword "\s*ldelem\.ref\s*"
syntax match ilasmKeyword "\s*ldelem\.r8\s*"
syntax match ilasmKeyword "\s*ldelem\.r4\s*"
syntax match ilasmKeyword "\s*ldelem\.i\s*"
syntax match ilasmKeyword "\s*ldelem\.u8\s*"
syntax match ilasmKeyword "\s*ldelem\.i8\s*"
syntax match ilasmKeyword "\s*ldelem\.u4\s*"
syntax match ilasmKeyword "\s*ldelem\.i4\s*"
syntax match ilasmKeyword "\s*ldelem\.u2\s*"
syntax match ilasmKeyword "\s*ldelem\.i2\s*"
syntax match ilasmKeyword "\s*ldelem\.u1\s*"
syntax match ilasmKeyword "\s*ldelem\.i1\s*"
syntax match ilasmKeyword "\s*ldelema\s*"
syntax match ilasmKeyword "\s*ldlen\s*"
syntax match ilasmKeyword "\s*newarr\s*"
syntax match ilasmKeyword "\s*box\s*"
syntax match ilasmKeyword "\s*conv\.ovf\.u\.un\s*"
syntax match ilasmKeyword "\s*conv\.ovf\.i\.un\s*"
syntax match ilasmKeyword "\s*conv\.ovf\.u8\.un\s*"
syntax match ilasmKeyword "\s*conv\.ovf\.u4\.un\s*"
syntax match ilasmKeyword "\s*conv\.ovf\.u2\.un\s*"
syntax match ilasmKeyword "\s*conv\.ovf\.u1\.un\s*"
syntax match ilasmKeyword "\s*conv\.ovf\.i8\.un\s*"
syntax match ilasmKeyword "\s*conv\.ovf\.i4\.un\s*"
syntax match ilasmKeyword "\s*conv\.ovf\.i2\.un\s*"
syntax match ilasmKeyword "\s*conv\.ovf\.i1\.un\s*"
syntax match ilasmKeyword "\s*stobj\s*"
syntax match ilasmKeyword "\s*stsfld\s*"
syntax match ilasmKeyword "\s*ldsflda\s*"
syntax match ilasmKeyword "\s*ldsfld\s*"
syntax match ilasmKeyword "\s*stfld\s*"
syntax match ilasmKeyword "\s*ldflda\s*"
syntax match ilasmKeyword "\s*ldfld\s*"
syntax match ilasmKeyword "\s*throw\s*"
syntax match ilasmKeyword "\s*unbox\s*"
syntax match ilasmKeyword "\s*conv\.r\.un\s*"
syntax match ilasmKeyword "\s*isinst\s*"
syntax match ilasmKeyword "\s*castclass\s*"
syntax match ilasmKeyword "\s*newobj\s*"
syntax match ilasmKeyword "\s*ldstr\s*"
syntax match ilasmKeyword "\s*cpobj\s*"
syntax match ilasmKeyword "\s*call\s*"
syntax match ilasmKeyword "\s*callvirt\s*"
syntax match ilasmKeyword "\s*calli\s*"
syntax match ilasmKeyword "\s*conv\.u8\s*"
syntax match ilasmKeyword "\s*conv\.u4\s*"
syntax match ilasmKeyword "\s*conv\.r8\s*"
syntax match ilasmKeyword "\s*conv\.r4\s*"
syntax match ilasmKeyword "\s*conv\.i8\s*"
syntax match ilasmKeyword "\s*conv\.i4\s*"
syntax match ilasmKeyword "\s*conv\.i2\s*"
syntax match ilasmKeyword "\s*conv\.i1\s*"
syntax match ilasmKeyword "\s*not\s*"
syntax match ilasmKeyword "\s*neg\s*"
syntax match ilasmKeyword "\s*shr\.un\s*"
syntax match ilasmKeyword "\s*shr\s*"
syntax match ilasmKeyword "\s*shl\s*"
syntax match ilasmKeyword "\s*xor\s*"
syntax match ilasmKeyword "\s*or\s*"
syntax match ilasmKeyword "\s*and\s*"
syntax match ilasmKeyword "\s*rem\.un\s*"
syntax match ilasmKeyword "\s*rem\s*"
syntax match ilasmKeyword "\s*div\.un\s*"
syntax match ilasmKeyword "\s*div\s*"
syntax match ilasmKeyword "\s*mul\s*"
syntax match ilasmKeyword "\s*sub\s*"
syntax match ilasmKeyword "\s*add\s*"
syntax match ilasmKeyword "\s*stind\.r8\s*"
syntax match ilasmKeyword "\s*stind\.r4\s*"
syntax match ilasmKeyword "\s*stind\.i8\s*"
syntax match ilasmKeyword "\s*stind\.i4\s*"
syntax match ilasmKeyword "\s*stind\.i2\s*"
syntax match ilasmKeyword "\s*stind\.i1\s*"
syntax match ilasmKeyword "\s*stind\.ref\s*"
syntax match ilasmKeyword "\s*ldind\.ref\s*"
syntax match ilasmKeyword "\s*ldind\.r8\s*"
syntax match ilasmKeyword "\s*ldind\.r4\s*"
syntax match ilasmKeyword "\s*ldind\.i\s*"
syntax match ilasmKeyword "\s*ldind\.u8\s*"
syntax match ilasmKeyword "\s*ldind\.i8\s*"
syntax match ilasmKeyword "\s*ldind\.u4\s*"
syntax match ilasmKeyword "\s*ldind\.i4\s*"
syntax match ilasmKeyword "\s*ldind\.u2\s*"
syntax match ilasmKeyword "\s*ldind\.i2\s*"
syntax match ilasmKeyword "\s*ldind\.u1\s*"
syntax match ilasmKeyword "\s*ldind\.i1\s*"
syntax match ilasmKeyword "\s*switch\s*"
syntax match ilasmKeyword "\s*blt\.un\s*"
syntax match ilasmKeyword "\s*ble\.un\s*"
syntax match ilasmKeyword "\s*bgt\.un\s*"
syntax match ilasmKeyword "\s*bge\.un\s*"
syntax match ilasmKeyword "\s*bne\.un\s*"
syntax match ilasmKeyword "\s*blt\s*"
syntax match ilasmKeyword "\s*ble\s*"
syntax match ilasmKeyword "\s*bgt\s*"
syntax match ilasmKeyword "\s*bge\s*"
syntax match ilasmKeyword "\s*beq\s*"
syntax match ilasmKeyword "\s*brinst\s*"
syntax match ilasmKeyword "\s*brtrue\s*"
syntax match ilasmKeyword "\s*brzero\s*"
syntax match ilasmKeyword "\s*brnull\s*"
syntax match ilasmKeyword "\s*brfalse\s*"
syntax match ilasmKeyword "\s*br\s*"
syntax match ilasmKeyword "\s*blt\.un\.s\s*"
syntax match ilasmKeyword "\s*ble\.un\.s\s*"
syntax match ilasmKeyword "\s*bgt\.un\.s\s*"
syntax match ilasmKeyword "\s*bge\.un\.s\s*"
syntax match ilasmKeyword "\s*bne\.un\.s\s*"
syntax match ilasmKeyword "\s*blt\.s\s*"
syntax match ilasmKeyword "\s*ble\.s\s*"
syntax match ilasmKeyword "\s*bgt\.s\s*"
syntax match ilasmKeyword "\s*bge\.s\s*"
syntax match ilasmKeyword "\s*beq\.s\s*"
syntax match ilasmKeyword "\s*brinst\.s\s*"
syntax match ilasmKeyword "\s*brtrue\.s\s*"
syntax match ilasmKeyword "\s*brzero\.s\s*"
syntax match ilasmKeyword "\s*brnull\.s\s*"
syntax match ilasmKeyword "\s*brfalse\.s\s*"
syntax match ilasmKeyword "\s*br\.s\s*"
syntax match ilasmKeyword "\s*ret\s*"
syntax match ilasmKeyword "\s*jmp\s*"
syntax match ilasmKeyword "\s*pop\s*"
syntax match ilasmKeyword "\s*dup\s*"
syntax match ilasmKeyword "\s*ldc\.r8\s*"
syntax match ilasmKeyword "\s*ldc\.r4\s*"
syntax match ilasmKeyword "\s*ldc\.i8\s*"
syntax match ilasmKeyword "\s*ldc\.i4\s*"
syntax match ilasmKeyword "\s*ldc\.i4\.s\s*"
syntax match ilasmKeyword "\s*ldc\.i4\.8\s*"
syntax match ilasmKeyword "\s*ldc\.i4\.7\s*"
syntax match ilasmKeyword "\s*ldc\.i4\.6\s*"
syntax match ilasmKeyword "\s*ldc\.i4\.5\s*"
syntax match ilasmKeyword "\s*ldc\.i4\.4\s*"
syntax match ilasmKeyword "\s*ldc\.i4\.3\s*"
syntax match ilasmKeyword "\s*ldc\.i4\.2\s*"
syntax match ilasmKeyword "\s*ldc\.i4\.1\s*"
syntax match ilasmKeyword "\s*ldc\.i4\.0\s*"
syntax match ilasmKeyword "\s*ldc\.i4\.M1\s*"
syntax match ilasmKeyword "\s*ldc\.i4\.m1\s*"
syntax match ilasmKeyword "\s*ldnull\s*"
syntax match ilasmKeyword "\s*stloc\.s\s*"
syntax match ilasmKeyword "\s*ldloca\.s\s*"
syntax match ilasmKeyword "\s*ldloc\.s\s*"
syntax match ilasmKeyword "\s*starg\.s\s*"
syntax match ilasmKeyword "\s*ldarga\.s\s*"
syntax match ilasmKeyword "\s*ldarg\.s\s*"
syntax match ilasmKeyword "\s*stloc\.3\s*"
syntax match ilasmKeyword "\s*stloc\.2\s*"
syntax match ilasmKeyword "\s*stloc\.1\s*"
syntax match ilasmKeyword "\s*stloc\.0\s*"
syntax match ilasmKeyword "\s*ldloc\.3\s*"
syntax match ilasmKeyword "\s*ldloc\.2\s*"
syntax match ilasmKeyword "\s*ldloc\.1\s*"
syntax match ilasmKeyword "\s*ldloc\.0\s*"
syntax match ilasmKeyword "\s*ldarg\.3\s*"
syntax match ilasmKeyword "\s*ldarg\.2\s*"
syntax match ilasmKeyword "\s*ldarg\.1\s*"
syntax match ilasmKeyword "\s*ldarg\.0\s*"
syntax match ilasmKeyword "\s*break\s*"
syntax match ilasmKeyword "\s*nop\s*"


syntax keyword ilasmBool true false

syntax keyword ilasmDeclaration public private privatescope family assembly famandassem famorassem cil cdecl stdcall thiscall fastcall managed unmanaged auto ansi extends static valuetype explicit init vararg extern at initonly rtspecialname marchal marshal literal notserialized specialname request demand assert deny permitonly linkcheck inheritcheck reqmin reqopt reqrefuse prejitgrant noncasdemand noncaslinkdemand noncasinheritance fromunmanaged callmostderived unicode value enum interface sealed abstract sequential autochar import serializable beforefieldinit nested virtual hidebysig newslot default instance runtime class property

syntax match ilasmTypeParam "!!*[A-Za-z0-9_]*"

syntax match ilasmMDToken "\(\[[a-zA-Z_][a-zA-Z0-9_\.\-\/]*\]\)\?[A-Za-z_\-`\$][A-Za-z0-9_\+\.\-`\$]*\.[A-Za-z0-9_\+\.\-`\$]*\(\s*::\s*[A-Za-z_][A-Za-z0-9_\-\+`\$]*\)\?"
syntax match ilasmMDToken "::\s*[A-Za-z_][A-Za-z0-9_\-\.\+`\$]*"



"                          except \\\" ('\"') (or \\\')
syntax match ilasmString "\".*\""
syntax match ilasmString "'.*'"

syntax match ilasmComment "\/\/.*$"
syntax match ilasmComment "\/\*.*\*\/"

syntax match ilasmLabel "^\s*[a-zA-Z_][a-zA-Z0-9_]*:"
syntax match ilasmString /"[^"]*"/
" preceded with a . -> part of an instruction (eg. ldarg.0)
" preceded with a ` -> part of a generic typename
"     (eg. [mscorlib]System.Collections.Generic.List`1<!!T>)
syntax match ilasmNumber "[0-9]\+"
                         " except [\.`] at beginning
syntax match ilasmNumber "0x[A-Fa-f0-9]\+"

syntax match ilasmVersion "[0-9]\(:[0-9]\)\{3\}"
syntax match ilasmBlob    "[A-Fa-f0-9]\{2\}\s\(\s[A-Fa-f0-9]\{2\}\)*"

if !exists("did_ilasm_syntax_inits")
    let did_ilasm_syntax_inits=1
    highlight link ilasmComment Comment
    highlight link ilasmLabel Label
    highlight link ilasmString String
    highlight link ilasmType Type

    highlight link ilasmDirective Keyword
    " PreProc
    highlight link ilasmDeclaration Keyword

    highlight link ilasmMDToken EndOfBuffer
    highlight link ilasmTypeParam Type

    highlight link ilasmKeyword Function

    highlight link ilasmBool Boolean

    highlight link ilasmNumber Number
    highlight link ilasmVersion Number
    highlight link ilasmBlob Number
endif
let b:current_syntax="ilasm"

