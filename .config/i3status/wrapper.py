#!/usr/bin/env python
#-*- coding: utf-8 -*-

# This script is a simple wrapper which prefixes each i3status line with custom
# information. It is a python reimplementation of:
# http://code.stapelberg.de/git/i3status/tree/contrib/wrapper.pl
#
# To use it, ensure your ~/.i3status.conf contains this line:
#     output_format = "i3bar"
# in the 'general' section.
# Then, in your ~/.i3/config, use:
#     status_command i3status | ~/i3status/contrib/wrapper.py
# In the 'bar' section.
#
# In its current version it will display the cpu frequency governor, but you
# are free to change it to display whatever you like, see the comment in the
# source code below.
#
# © 2012 Valentin Haenel <valentin.haenel@gmx.de>
#
# This program is free software. It comes without any warranty, to the extent
# permitted by applicable law. You can redistribute it and/or modify it under
# the terms of the Do What The Fuck You Want To Public License (WTFPL), Version
# 2, as published by Sam Hocevar. See http://sam.zoy.org/wtfpl/COPYING for more
# details.
#
# NOTE: I (PoroCYon) have added the part that fetches the currently played song
# from scenesat. This part is licensed under the WTFPL as well.

import sys
import json
import urllib.request
import datetime

# enable optshere
option_scenesat = True

def print_scenesat(j, track_start, track_length, dnow):
    if track_length <= 0:
        return "-HTTP error-"

    now = j["now"]
    durs = now["duration"] % 60
    durm = int((now["duration"] - durs) / 60)

    durss = str(durs)
    if len(durss) == 1:
        durss = "0" + durss

    cur = dnow - track_start

    if j["other"]["islive"] != 0:
        ret = "LIVE: " + j["live"]["host"] + " - " + j["live"]["name"]
    else:
        ret = now["artist"] + " - " + now["title"]

    curss = str(cur.seconds % 60)
    if len(curss) == 1:
        curss = "0" + curss
    curm = int((cur.seconds - cur.seconds % 60) / 60)

    ret = ret + " [" + str(curm) + ":" + curss + "/" + str(durm) + ":" + durss + "]"

    return ret

def get_scenesat():
    try:
        url = urllib.request.urlopen("https://scenesat.com/api/quicklist/json", timeout=2)
    except e:
        return (json.loads("{}"), datetime.datetime.now(), 0, datetime.datetime.now())

    j = json.loads(url.read().decode())["tunes"]
    url.close()

    now = j["now"]

    nstart = datetime.datetime.strptime(j["next"]["startingat"], "%H:%M:%S")
    nstart = nstart - datetime.timedelta(seconds = j["next"]["leftuntil"])
    snow = datetime.datetime.strptime(j["next"]["timenow"], "%Y-%m-%d %H:%M")
    # actual now (according to scenesat)
    snow = snow + datetime.timedelta(seconds = nstart.second)

    track_start  = datetime.datetime.strptime(now["startedat"], "%Y-%m-%d %H:%M:%S")
    track_length = now["duration"]

    return (j, track_start, track_length, snow)

def print_line(message):
    """ Non-buffered printing to stdout. """
    sys.stdout.write(message + '\n')
    sys.stdout.flush()

def read_line():
    """ Interrupted respecting reader for stdin. """
    # try reading a line, removing any extra whitespace
    try:
        line = sys.stdin.readline().strip()
        # i3status sends EOF, or an empty line
        if not line:
            sys.exit(3)
        return line
    # exit on ctrl-c
    except KeyboardInterrupt:
        sys.exit()

if __name__ == '__main__':
    track_j = None
    now = None
    track_length = 0
    track_start = datetime.datetime.now()
    tdel = datetime.timedelta(seconds = 0)

    # Skip the first line which contains the version header.
    print_line(read_line())

    # The second line contains the start of the infinite array.
    print_line(read_line())

    while True:
        line, prefix = read_line(), ''
        # ignore comma at start of lines
        if line.startswith(','):
            line, prefix = line[1:], ','

        j = json.loads(line)

        # insert information into the start of the json, but could be anywhere
        if option_scenesat:
            tnow = datetime.datetime.now()
            if now == None or track_end <= tnow:
                track_j, track_start, track_length, snow = get_scenesat()
                tdel = tnow - snow
            track_end = track_start + datetime.timedelta(seconds = track_length) + tdel

            ss = print_scenesat(track_j, track_start, track_length, tnow - tdel)
            j.insert(0, {'full_text' : '%s' % ss, 'name' : 'scenesat'})

        # and echo back new encoded json
        print_line(prefix+json.dumps(j))

