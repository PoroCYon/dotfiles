#!/usr/bin/env bash

allbgs=($(find "/home/poro/Downloads/Wallpapers/" -mindepth 1 -maxdepth 1 -type f))
bgf="${allbgs[$RANDOM % ${#allbgs[@]}]}"

if ! [ -z "$SWAYSOCK" ]; then
    swaymsg -t command output "*" bg "$bgf" fill >/dev/null
else
    feh --bg-fill "$bgf" --no-fehbg
fi

if ! [ -z "$SWAYSOCK" ]; then
    ~/.config/sway/alphard/fixpos.sh
fi

