#!/usr/bin/env bash
# 09F911029D74E35BD84156C5635688C0

[ -f "/etc/profile.d/locale.sh" ] && source /etc/profile.d/locale.sh

export DOTNET_CLI_TELEMETRY_OPTOUT=1

export LC_COLLATE='C'
export LC_TIME='nl_BE.UTF-8'

export XKB_DEFAULT_LAYOUT='be'
export XKB_DEFAULT_OPTIONS='ctrl:swapcaps,combine:menu'
export WM='sway'
export EDITOR='nvim'
export SUDO_EDITOR='rvim'
export GPGKEY='D90A5850F019571221E04D446A38DA954AA08519'
export PAGER='less'

export MANROFFOPT='-c'
if ! [ -z "$TERM" ]; then
    export LESS_TERMCAP_mb=$(tput bold; tput setaf 2)
    export LESS_TERMCAP_md=$(tput bold; tput setaf 6)
    export LESS_TERMCAP_me=$(tput sgr0)
    export LESS_TERMCAP_so=$(tput bold; tput setaf 3; tput setab 4)
    export LESS_TERMCAP_se=$(tput rmso; tput sgr0)
    export LESS_TERMCAP_us=$(tput smul; tput bold; tput setaf 7)
    export LESS_TERMCAP_ue=$(tput rmul; tput sgr0)
    export LESS_TERMCAP_mr=$(tput rev)
    export LESS_TERMCAP_mh=$(tput dim)
fi

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"

# force wayland
#export GDK_BACKEND=wayland
#export QT_QPA_PLATFORM=wayland-egl
#export CLUTTER_BACKEND=wayland
#export SDL_VIDEODRIVER=wayland
export _JAVA_AWT_WM_NONREPARENTING=1
export TERM_PROGRAM=$TERM

alias ping6="ping -6"
#alias ld="ls -d"
alias ll="ls -l"
alias la="ls -A"
alias lla="ls -la"
alias llh="ls -lh"
alias llha="ls -lha"
alias hd="hexdump -C"
alias md="mkdir"
alias openmpt123="openmpt123 --driver portaudio"
alias openmpt="openmpt123"
alias pstree="pstree -agpsu"
#alias stack="stack --allow-different-user"
alias hl="highlight -O ansi -S"
alias fbv="fbv -c"
alias ls="ls --color=auto"
alias desmume='desmume --jit-size 100 --jit-enable --3d-texture-upscale 1'

#git config --global alias.l "log --graph --oneline"
#git config --global alias.undo '!f() { \
#    git reset --hard $(git rev-parse --abbrev-ref HEAD)@{${1-1}}; \
#}; f'

if [ -f "/etc/profile.d/devkit-env.sh" ]; then
    . "/etc/profile.d/devkit-env.sh"
fi

# get openmpt123 to work
if [ -z "$LD_LIBRARY_PATH" ]; then
    export LD_LIBRARY_PATH="$HOME/.local/lib:/usr/local/lib"
else
    export LD_LIBRARY_PATH="$HOME/.local/lib:/usr/local/lib/:$LD_LIBRARY_PATH"
fi

if [ -z "$MONO_PATH" ]; then
    export MONO_PATH="$HOME/.local/lib"
else
    export MONO_PATH="$HOME/.local/lib:$MONO_PATH"
fi

export GOPATH="$HOME/src/go"
if [ -z "$PATH" ]; then
    export PATH="$HOME/.local/bin:$HOME/.local/sbin:$HOME/.cargo/bin:/usr/local/bin:/usr/bin:/usr/sbin:$HOME/.local/games:$GOPATH/bin"
else
    export PATH="$HOME/.local/bin:$HOME/.local/sbin:$HOME/.cargo/bin:$PATH:/usr/sbin:$HOME/.local/games:$GOPATH/bin"
fi

if [ -z "$MANPATH" ]; then
    export MANPATH="$HOME/.local/share/man:/usr/local/share/man:/usr/share/man"
else
    export MANPATH="$HOME/.local/share/man:/usr/local/share/man:$MANPATH"
fi

if [ -z "$PKG_CONFIG_PATH" ]; then
    export PKG_CONFIG_PATH="$HOME/.local/lib/pkgconfig:/usr/lib/pkgconfig"
else
    export PKG_CONFIG_PATH="$HOME/.local/lib/pkgconfig:$PKG_CONFIG_PATH"
fi

if [ "$TERM" == "linux" ]; then
    # kill the damned bell
    setterm -blength 0
    # load belgian AZERTY keymap
    #loadkeys be-latin1
fi

todo() {
    if which rg >/dev/null 2>&1; then
        rg -C 2 -ipF -e TODO -e FIXME
    else
        grep -C 2 -niHTF -R -e TODO -e FIXME
    fi
}
export -f todo

urlencode() {
    # urlencode <string>
    old_lc_collate=$LC_COLLATE
    LC_COLLATE=C

    local length="${#1}"
    for (( i = 0; i < length; i++ )); do
        local c="${1:i:1}"
        case $c in
            [a-zA-Z0-9.~_-]) printf "$c" ;;
            *) printf '%%%02X' "'$c" ;;
        esac
    done

    LC_COLLATE=$old_lc_collate
}

duckduckgo() {
    lynx --underline-links "https://duckduckgo.com/lite?q=$(urlencode "$@")"
}
export -f duckduckgo
alias ddg="duckduckgo"
vlasisku() {
    lynx --underline-links "http://vlasisku.lojban.org/$(urlencode "$@")"
    #lynx --underline-links "https://vlasisku.alexburka.com/$(urlencode "$@")"
}
export -f vlasisku
alias vs="vlasisku"

if [ "$HOSTNAME" == "" ]; then
    export HOSTNAME=$(cat /etc/hostname)
fi

export PS1="\u@\h:\W$ "

# do machine-specific stuff
MDEPPR="$HOME/.profile-$HOSTNAME"
if [ -f "$MDEPPR" ]; then
    . "$MDEPPR"
fi

# export PATH="$(echo $PATH | tr : '\n' | sort -u | tr '\n' :)"

