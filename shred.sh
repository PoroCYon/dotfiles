#!/usr/bin/env bash

ls -dA1 \
    ~/.*_history \
    ~/.log \
    ~/*/history \
    ~/.weechat/logs/*.weechatlog \
    ~/.*/*/history \
    ~/*_hist \
    ~/.cache/rofi* \
    2>/dev/null \
    | xargs -tr shred -u
#    ~/*/*/history \

