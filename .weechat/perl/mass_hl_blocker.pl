# Mass highlight blocker for WeeChat by arza <arza@arza.us>, distributed freely and without any warranty, licensed under GPL3 <http://www.gnu.org/licenses/gpl.html>
# with edits by PoroCYon, some stuff taken from https://weechat.org/scripts/source/notify_send.pl.html/ because I suck at perl

#use strict;
use warnings;

weechat::register('mass_hl_blocker', 'arza <arza\@arza.us>, PoroCYon', '0.2', 'GPL3', 'Block mass highlights', '', '');

my $version=weechat::info_get('version_number', '') || 0;

my %settings_default=(
	"limit"  => [ 5 , 'minimum amount of nicks in line to disable highlight' ],
	"wlbuf"  => [ '', 'buffer whitelist'   ],
	"wlserv" => [ '', 'server whitelist'   ],
	"wlchan" => [ '', 'channel whitelist'  ],
	"wlnick" => [ '', 'nick whitelist'     ]
);
my %settings=();

# ----------

sub config_changed {
	my ($pointer, $name, $value) = @_;
	$name = substr($name, length("plugins.var.perl.mass_hl_blocker."), length($name));
	$settings{$name} = $value;
	return weechat::WEECHAT_RC_OK;
}

sub config_init {
	my $version = weechat::info_get("version_number", "") || 0;
	foreach my $option (keys %settings_default) {
		if (!weechat::config_is_set_plugin($option)) {
			weechat::config_set_plugin($option, $settings_default{$option}[0]);
			$settings{$option} = $settings_default{$option}[0];
		} else {
			$settings{$option} = weechat::config_get_plugin($option);
		}
		if ($version >= 0x00030500) {
			weechat::config_set_desc_plugin($option,
				$settings_default{$option}[1] . " (default: \""
				. $settings_default{$option}[0] . "\")");
		}
	}
}
config_init;
weechat::hook_config('plugins.var.perl.mass_highlight_block.*', 'config_changed', '');

# ----------

sub on_msg {
	my $message=$_[3];
	$_[2]=~/(\S+);(\S+)\.(\S+);(\S+)/ || return $message;
	my ($plugin, $server, $channel, $tags) = ($1, $2, $3, $4);
	index($message, weechat::info_get('irc_nick', $server)) != -1 && index($tags, 'notify_message') != -1 && index($tags, 'no_highlight') == -1 || return $message;

	my $buffer = do {
		my $infolist = weechat::infolist_get("buffer", weechat::current_buffer, "");
		weechat::infolist_next($infolist);
		my $buffers_name = weechat::infolist_string($infolist, "name");
		weechat::infolist_free($infolist);
		$buffers_name;
	};

	# buffer in whitelist -> ok
	index($settings{'wlbuf' }.',', $buffer .',') == -1 || return $message;

	# server in whitelist -> ok
	index($settings{'wlserv'}.',', $server .',') == -1 || return $message;

	# channel in whitelist -> ok
	index($settings{'wlchan'}.',', $channel.',') == -1 || return $message;

	# nick in whitelist -> ok # TODO
	#$wlnick{$nick_TODO} && return $message;

	# check limit
	my $limit = $settings{'limit'};
	my $count = 0;
	foreach my $word (split(' ', $message)) {
		my $infolist = weechat::infolist_get('irc_nick', '', "$server,$channel,$word");
		if ($infolist) { $count++; }
		weechat::infolist_free($infolist);
	}

	if ($count >= $limit) {
		weechat::print_date_tags(weechat::buffer_search($plugin, "$server.$channel"), 0, "$tags,no_highlight", $message);
		return '';
	}

	return $message;
}
weechat::hook_modifier('2000|weechat_print', 'on_msg', '');

